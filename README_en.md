# Hanakopatch

## What's This?

Hanakopatch is a bug fix patch for [TH12.5 Double Spoiler](https://en.touhouwiki.net/wiki/Double_Spoiler) Scene 12-6, 正体不明「厠の花子さん」(Unidentified "Ms. Hanako of the Toilet").

The following bugs will be fixed by hanakopatch:

 * Replay desynchronization.
 * Blue bullets may be emitted from other than top of screen.

Although Scene 12-6 is famous for ["random death" bug](https://en.touhouwiki.net/wiki/Double_Spoiler/Gameplay#Bugs), I can't prove hanakopatch fixes this bug....

## Usage

Run [SpoilerAL](http://wcs.main.jp/index/software/spal/) with `th125_hanakopatch.ssg`.

## Compatibility of Replay

| Save replay on | Show replay on | ok/NG |
|----------------|----------------|-------|
| DS ver 1.00a | DS ver 1.00a | NG |
| DS ver 1.00a | DS ver 1.00a + hanakopatch | NG |
| DS ver 1.00a + hanakopatch | DS ver 1.00a | NG |
| DS ver 1.00a + hanakopatch | DS ver 1.00a + hanakopatch | ok |

## Bug Mechanism

### TL;DR

Uninitialized fields pollute the initial position of blue bullets, it causes problems.

### Detail

A position of blue bullets is initialized to the value of field "B" of a certain object "A".
Object A itself is initialized by the code in address 0x410240-0x410482, **but field B is not**.
So the value of field B is equal to the original content of memory where object A is allocated.

Object A is allocated in the heap space, where is not always zero-cleared and may be polluted by old data.
**It means that field B and thus the initial position of blue bullets are potentially polluted.**

A concrete value of field B can vary for each gameplay and replay.
Accordingly, the following phenomena will occur:

 * The initial position of blue bullets may change for each gameplay and replay.
 * The above change affects the random number generation, so other elements (e.g., lazer and enemy) may also change.

## Hanakopatch Mechanism

Hanakopatch resolves these problems by initializing field B appropriately.

In detail, hanakopatch rewrites machine code like below.

```
; Before
00410F4E   5B               POP EBX
00410F4F   C2 0400          RETN 4
00410F52   CC               INT3
00410F53   CC               INT3
00410F54   CC               INT3
00410F55   CC               INT3
00410F56   CC               INT3
00410F57   CC               INT3
00410F58   CC               INT3

; After
00410F4E   895F 10          MOV DWORD PTR DS:[EDI+10],EBX ; initialize fields to 0
00410F51   895F 14          MOV DWORD PTR DS:[EDI+14],EBX
00410F54   5B               POP EBX
00410F55   C2 0400          RETN 4
00410F58   CC               INT3
```

Although field B is potentially polluted, in many cases the actual value of field B is 0.
Therefore, hanakopatch always initializes field B to 0 to avoid the blue bullet position change and the replay desynchronization.
